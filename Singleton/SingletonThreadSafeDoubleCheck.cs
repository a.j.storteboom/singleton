namespace Singleton
{
    public class SingletonThreadSafeDoubleCheck
    {
        private SingletonThreadSafeDoubleCheck()
        {
        }

        private static readonly object padlock = new object();
        private static SingletonThreadSafeDoubleCheck _instance;

        public static SingletonThreadSafeDoubleCheck GetInstance()
        {
            if (_instance == null)
            {
                lock (padlock)
                {
                    if (_instance == null)
                    {
                        _instance = new SingletonThreadSafeDoubleCheck();
                    }
                }
            }

            return _instance;
        }
    }
}