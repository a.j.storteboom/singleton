using System;

namespace Singleton
{
    public class Logger
    {
        private string Channel = "app";
        private static readonly object Padlock = new object();
        private static Logger _instance;
        private DateTime DateTime;

        private Logger()
        {
        }

        public static Logger GetInstance()
        {
            if (_instance == null)
            {
                lock (Padlock)
                {
                    if (_instance == null)
                    {
                        _instance = new Logger();
                    }
                }
            }

            return _instance;
        }

        public void Log(string message)
        {
            string logTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            Console.WriteLine("[" + logTime + "] " + message);
        }
    }
}