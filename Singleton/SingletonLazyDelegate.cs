using System;

namespace Singleton
{
    public class SingletonLazyDelegate
    {
        private static readonly Lazy<SingletonLazyDelegate> lazy =
            new Lazy<SingletonLazyDelegate>(() => new SingletonLazyDelegate());

        public static SingletonLazyDelegate GetInstance()
        {
            return lazy.Value;
        }
    }
}