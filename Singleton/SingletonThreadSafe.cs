namespace Singleton
{
    public class SingletonThreadSafe
    {
        private SingletonThreadSafe()
        {
        }

        private static readonly object Padlock = new object();
        
        private static SingletonThreadSafe _instance;

        public static SingletonThreadSafe GetInstance()
        {
            lock (Padlock)
            {
                if (_instance == null)
                {
                    _instance = new SingletonThreadSafe();
                }
            }

            return _instance;
        }
    }
}