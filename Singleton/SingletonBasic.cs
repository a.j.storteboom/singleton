namespace Singleton
{
    public class SingletonBasic
    {
        private SingletonBasic()
        {
        }

        private static SingletonBasic _instance;

        public static SingletonBasic GetInstance()
        {
            if (_instance == null)
            {
                _instance = new SingletonBasic();
            }

            return _instance;
        }
    }
}