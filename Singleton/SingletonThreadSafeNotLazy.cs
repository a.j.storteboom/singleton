namespace Singleton
{
    public class SingletonThreadSafeNotLazy
    {
        private static readonly SingletonThreadSafeNotLazy _instance = new SingletonThreadSafeNotLazy();

        static SingletonThreadSafeNotLazy()
        {
        }

        private SingletonThreadSafeNotLazy()
        {
        }

        public static SingletonThreadSafeNotLazy GetInstance()
        {
            return _instance;
        }
    }
}