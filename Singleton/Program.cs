﻿using System;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger logger = Logger.GetInstance();

            SingletonBasic basicInstanceOne = SingletonBasic.GetInstance();
            SingletonBasic basicInstanceTwo = SingletonBasic.GetInstance();

            if (basicInstanceOne == basicInstanceTwo)
            {
                logger.Log("Basic: Success");
            }
            else
            {
                logger.Log("Basic: Error");
            }


            SingletonThreadSafe threadSafeInstanceOne = SingletonThreadSafe.GetInstance();
            SingletonThreadSafe threadSafeInstanceTwo = SingletonThreadSafe.GetInstance();

            if (threadSafeInstanceOne == threadSafeInstanceTwo)
            {
                logger.Log("Thread safe: Success");
            }
            else
            {
                logger.Log("Thread safe: Error");
            }


            SingletonThreadSafeDoubleCheck threadSafeDoubleCheckInstanceOne =
                SingletonThreadSafeDoubleCheck.GetInstance();
            SingletonThreadSafeDoubleCheck threadSafeDoubleCheckInstanceTwo =
                SingletonThreadSafeDoubleCheck.GetInstance();

            if (threadSafeDoubleCheckInstanceOne == threadSafeDoubleCheckInstanceTwo)
            {
                logger.Log("Thread safe double check: Success");
            }
            else
            {
                logger.Log("Thread safe double check: Error");
            }


            SingletonThreadSafeNotLazy threadSafeNotLazyInstanceOne = SingletonThreadSafeNotLazy.GetInstance();
            SingletonThreadSafeNotLazy threadSafeNotLazyInstanceTwo = SingletonThreadSafeNotLazy.GetInstance();

            if (threadSafeNotLazyInstanceOne == threadSafeNotLazyInstanceTwo)
            {
                logger.Log("Thread safe not lazy check: Success");
            }
            else
            {
                logger.Log("Thread safe not lazy check: Error");
            }


            SingletonLazyDelegate lazyDelegateInstanceOne = SingletonLazyDelegate.GetInstance();
            SingletonLazyDelegate lazyDelegateInstanceTwo = SingletonLazyDelegate.GetInstance();

            if (lazyDelegateInstanceOne == lazyDelegateInstanceTwo)
            {
                logger.Log("Lazy delegate check: Success");
            }
            else
            {
                logger.Log("Lazy delegate check: Error");
            }
        }
    }
}