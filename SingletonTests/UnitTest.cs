using NUnit.Framework;
using Singleton;

namespace SingletonTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }
        
        [Test]
        public void TestSingletonBasic()
        {
            var firstCall = SingletonBasic.GetInstance();
            var secondCall = SingletonBasic.GetInstance();

            Assert.AreEqual(firstCall, secondCall);
        }

        [Test]
        public void TestSingletonLogger()
        {
            var firstCall = Logger.GetInstance();
            var secondCall = Logger.GetInstance();

            Assert.AreEqual(firstCall, secondCall);
        }
        
        [Test]
        public void TestSingletonThreadSafe()
        {
            var firstCall = SingletonThreadSafe.GetInstance();
            var secondCall = SingletonThreadSafe.GetInstance();

            Assert.AreEqual(firstCall, secondCall);
        }
        
        [Test]
        public void TestSingletonThreadSafeDoubleCheck()
        {
            var firstCall = SingletonThreadSafeDoubleCheck.GetInstance();
            var secondCall = SingletonThreadSafeDoubleCheck.GetInstance();

            Assert.AreEqual(firstCall, secondCall);
        }
        
        [Test]
        public void TestSingletonThreadSafeNotLazy()
        {
            var firstCall = SingletonThreadSafeNotLazy.GetInstance();
            var secondCall = SingletonThreadSafeNotLazy.GetInstance();

            Assert.AreEqual(firstCall, secondCall);
        }
        
        [Test]
        public void TestSingletonLazyDelegate()
        {
            var firstCall = SingletonLazyDelegate.GetInstance();
            var secondCall = SingletonLazyDelegate.GetInstance();

            Assert.AreEqual(firstCall, secondCall);
        }
    }
}